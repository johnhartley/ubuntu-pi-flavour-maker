#!/usr/bin/env bash

########################################################################
#
# Copyright (C) 2015 Martin Wimpress <code@ubuntu-mate.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
########################################################################

FLAVOUR="${FLAVOUR:-ubuntu-minimal}"
FLAVOUR_NAME="${FLAVOUR_NAME:-Ubuntu}"
RELEASE="${RELEASE:-xenial}"
VERSION="${VERSION:-16.04}"
QUALITY="${QUALITY:-rc}"
TARGET_ARCH="${TARGET_ARCH:-armhf}"
TARGET_DIR="${TARGET_DIR:-.}"
OVERLAY_ROOT="${OVERLAY_ROOT:-0}"
if [ "$TARGET_ARCH" != "amd64" ]; then
  ARCHIVE="${ARCHIVE:-http://ports.ubuntu.com/}"
fi

# Either 'ext4' or 'f2fs'
FS_TYPE="${FS_TYPE:-ext4}"

# Either 4, 8 or 16
FS_SIZE=${FS_SIZE:-4}

# Either 0 or 1.
# - 0 don't make generic rootfs tarball
# - 1 make a generic rootfs tarball
MAKE_TARBALL=1

TARBALL="${FLAVOUR}-${VERSION}${QUALITY}-server-${TARGET_ARCH}-rootfs.tar.bz2"
IMAGE="${FLAVOUR}-${VERSION}${QUALITY}-server-${TARGET_ARCH}-raspberry-pi.img"
BASEDIR=${HOME}/PiFlavourMaker/${RELEASE}
BUILDDIR=${BASEDIR}/${FLAVOUR}
BASE_R=${BASEDIR}/base
DESKTOP_R=${BUILDDIR}/desktop
DEVICE_R=${BUILDDIR}/pi
ARCH=$(uname -m)
if [ "$ARCH" = "x86_64" ]; then
  ARCH=amd64
fi
export TZ=UTC


USERNAME="ubuntu"
OEM_CONFIG=0

# Override OEM_CONFIG here if required. Either 0 or 1.
# - 0 to hardcode a user.
# - 1 to use oem-config.
#OEM_CONFIG=1

if [ ${OEM_CONFIG} -eq 1 ]; then
    USERNAME="oem"
fi
